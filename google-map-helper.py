# GoogleMapHelper.py
# class and methods to do operations with google maps

import googlemaps  # // pip install googlemaps

from datetime import datetime
from pprint import pprint

import requests
import urllib.parse




class GoogleMapHelper:
    def __init__(self, api_key):
        self.map_client = googlemaps.Client(api_key)
        self.API_KEY = api_key


    # geocode an address: given dict with key: address, returns filled in keys from google:
    #  formatted_address, place_id, location (lat,long)
    #

    def geocode(self,add):
        address_geocoded = self.map_client.geocode(add['address'])
        add['formatted_address'] = address_geocoded[0]['formatted_address']
        add['place_id'] = address_geocoded[0]['place_id']
        add['location'] = address_geocoded[0]['geometry']['location']
        return(add)

    # geocode_addresses: given a list of addresses where each is a dictionary , geocodes them all 

    def geocode_address_list(self,address_list):
        return list(map(self.geocode,address_list))


    # placeid: takes address dict and returns placeid string
    @staticmethod
    def placeid(x):
        return "place_id:" + x['place_id']

    # takes dict with an address and checks if it is geocoded
    @staticmethod
    def is_address_geocoded(address):
        keys = address.keys()
        if 'place_id' in keys  and 'formatted_address' in keys and 'location' in keys :
            # check if values are not empty
            if len(address['place_id']) > 0 and len(address['formatted_address']) > 0 and address['location'] :
                return True
            else:
                return False
            return False
        return False


    def is_address_list_geocoded(self,address_list):
        return all(list(map(self.is_address_geocoded,address_list)))

    @staticmethod
    def marker_string(address_list):
        tstr = list(map(lambda x: str(x['location']['lat']) + "," + str(x['location']['lng']) , address_list))
        delimiter="|"
        return delimiter.join(tstr)


    # given a start_address, end_address, and list of waypoints, returns an optimum route
    # all addresses must be dictionaries and must contain geocode information 
    #

    def findoute(self,start_address, end_address, stops,optimize=True):
        # check if all addresses have place_id and formatted_addresses
        if not self.is_address_list_geocoded([start_address, end_address] +  stops):
            return { 'status': 'error', 'error': 'one or more addresses not geocoded' }

        # put in seqeunce number in stops
        i = 0
        for x in stops:
            x['old_seq'] = i
            i = i + 1

        waypoint_addresses = list(map(lambda x: self.placeid(x), stops))
        
        route = self.map_client.directions(self.placeid(start_address),
                                      self.placeid(end_address),
                                      waypoints = waypoint_addresses,
                                      optimize_waypoints=optimize,
                                      mode="driving"
        )
        polyline = route[0]['overview_polyline']['points']    

        # generate url for image

        # determine marker locations for waypoints
        waypoint_markers = self.marker_string(stops)

        # determine marker locations for start, end  locations
        stop_start_markers = self.marker_string([start_address,end_address])
        


        BASE_URL = "https://maps.googleapis.com/maps/api/staticmap?"
        url = BASE_URL +  "size=" + urllib.parse.quote("500x500") + "&path=enc:" + polyline +  "&markers=color:blue|size:small|" + urllib.parse.quote(waypoint_markers) +  "&markers=color:red!size:small|" + urllib.parse.quote(stop_start_markers) + "&key=" + self.API_KEY


        # now determine stops and reorder waypoints

        stop_address= []
        for leg in route[0]['legs']:
            stop_address.append(leg['start_address'])
        
        stop_address = stop_address[1:]    

        new_stops = []




        i = 0
        for x in stop_address:
            # find address dict
            results = [t  for t in stops if t['formatted_address'] == x]
            if len(results) > 0 :
                results[0]['seq'] = i
                i = i + 1
                new_stops.append(results[0])
            else:
                return { 'status': 'error', 'error': 'unable to match waypoint address' }
            

        # directions

        return_value = { 'status': "ok" , 
                         'route': route,
                         'url' : url,
                         'start': start_address,
                         'end': end_address,
                         'stops' : new_stops,
                         'directions' : route[0]['legs']
        }
        return return_value


# Pretty Print route and retrieve route map into file 
def pp_route(route,fname):
    print("Start:" + route['start']['formatted_address'])
    print("Stops:")
    stop_summary = list( map(lambda x: x['formatted_address'], route['stops']))
    pprint(stop_summary)
    print("End:" + route['end']['formatted_address'])





    # store route map in file
    response = requests.get(route['url'])
 
    # storing the response in a file (image)
    with open(fname, 'wb') as file:
        # writing data into the file
        file.write(response.content)
    print("Stored image in file: " + fname)                            


def test_googlemaphelper() :
    print("Starting demo of GoogleGoogleMapHelper");

    # read google maps api_key from text file
    with open('api-key.txt') as f:
        API_KEY = f.readline()
    f.close



    map = GoogleMapHelper(API_KEY)

    # create address list

    address_list = [
        {
            'description': 'Home of Vasee',
            'stoptype':  'start',
            'address':  "330 Beveridge Rd, Ridgewood, NJ 07450, USA",
            'formatted_address': "",
            'location': {},
            'place_id': ""
        },

        {
            'description': 'Work Address',
            'stoptype':  'end',
            'address':  "370 W Passaic St, 1st Flr., Rochelle Park, NJ 07662",
            'formatted_address': "",
            'location': {},
            'place_id': ""
        },    
    
        {
            'description': 'Vasees Sons School',
            'stoptype':  'stop',
            'address':  "The Phoenix Center, 16 Monsignor Owens Place, Nutley, NJ 07110",
            'formatted_address': "",
            'location': {},
            'place_id': ""
        },

        {
            'description': 'Ridgewood Bagel Shop',
            'stoptype':  'stop',
            'address':  "Ridgewood Hot Bagels, 110 N Maple Avenue. Ridgewood, NJ 07450",
            'formatted_address': "",
            'location': {},
            'place_id': ""
        } 
]

    # geocode the list
    geocoded_address_list = map.geocode_address_list(address_list)

    # create non optimzed route f

    print("FINDING ROUTE")
    route = map.findoute(geocoded_address_list[0],
                                    geocoded_address_list[0],
                                    [geocoded_address_list[1], geocoded_address_list[
2], geocoded_address_list[3]],False)
    pp_route(route,"routemap.png")


    print("")
    print("")
    # create optimized route for comparison
    print("FINDING OPTIMIZED ROUTE")
    optimized_route = map.findoute(geocoded_address_list[0],
                                    geocoded_address_list[0],
                                    [geocoded_address_list[1], geocoded_address_list[2], geocoded_address_list[3]], True)


    pp_route(optimized_route,"optimized_routemap.png")





if __name__ == "__main__":
    import sys
    # runtest(int(sys.argv[1]))
    test_googlemaphelper()





































